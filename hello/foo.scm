(define-module (hello foo)
  #:use-module (system foreign)
  #:use-module (system foreign-library))

(define libfoo (load-foreign-library "libfoo" #:global? #t))

(define-public (foo x y)
  ((pointer->procedure double (dynamic-func "foo" libfoo) `(,double ,double)) x y))
